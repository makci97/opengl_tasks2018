#include "Mesh.hpp"
#include "PerlinNoise.h"

#include <iostream>
#include <vector>
#include <exception>
#include <map>
#include <functional>
#include <numeric>

MeshPtr make_landscape(float size, unsigned int frequency, int num_octvaes, float persistence) {

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texcoords;

	PerlinNoise perlinNoise = PerlinNoise(num_octvaes, persistence);

	//Array for all triangle vertex normals
	std::vector<std::vector<std::vector<std::pair<int, glm::vec3>>>>  nonInterpNormals(frequency + 2,
		std::vector<std::vector<std::pair<int, glm::vec3>>>(frequency + 2, 
			std::vector<std::pair<int, glm::vec3>>()));

	// grid step
	float step = 2.f / frequency;

	// For nonInterpNormals
	int idx = 0;
	int ii = 0;
	int jj = 0;
	for (float i = -1., ii = 0; i < 1.; i += step, ++ii) {
		for (float j = -1., jj = 0; j < 1.; j += step, ++jj) {
			// Frist triangle
			glm::vec3 first = glm::vec3(i * size, j * size, perlinNoise.perlinNoise2D(i, j));
			glm::vec3 second = glm::vec3((i + step) * size, j * size, perlinNoise.perlinNoise2D(i + step, j));
			glm::vec3 third = glm::vec3(i * size, (j + step) * size, perlinNoise.perlinNoise2D(i, j + step));
			
			vertices.push_back(first);
			vertices.push_back(second);
			vertices.push_back(third);

			// Calculate normal
			glm::vec3 norm = glm::normalize(glm::cross(third - first, second - first));
			nonInterpNormals[ii][jj].push_back(std::make_pair(idx++, -norm));
			nonInterpNormals[ii+1][jj].push_back(std::make_pair(idx++, -norm));
			nonInterpNormals[ii][jj+1].push_back(std::make_pair(idx++, -norm));

			float tx = i + 1.f;
			float ty = j + 1.f;
			texcoords.push_back(glm::vec2(tx, ty));
			texcoords.push_back(glm::vec2(tx + step, ty));
			texcoords.push_back(glm::vec2(tx, ty + step));

			// Second triangle
			first = glm::vec3(i * size, (j + step) * size, perlinNoise.perlinNoise2D(i, j + step));
			second = glm::vec3((i + step) * size, (j + step) * size, perlinNoise.perlinNoise2D(i + step, j + step));
			third = glm::vec3((i + step) * size, j * size, perlinNoise.perlinNoise2D(i + step, j));

			vertices.push_back(first);
			vertices.push_back(second);
			vertices.push_back(third);

			norm = glm::normalize(glm::cross(third - first, second - first));
			nonInterpNormals[ii][jj + 1].push_back(std::make_pair(idx++, norm));
			nonInterpNormals[ii + 1][jj + 1].push_back(std::make_pair(idx++, norm));
			nonInterpNormals[ii + 1][jj].push_back(std::make_pair(idx++, norm));

			texcoords.push_back(glm::vec2(tx, ty + step));
			texcoords.push_back(glm::vec2(tx + step, ty + step));
			texcoords.push_back(glm::vec2(tx + step, ty));
		}
	}

	// Find interpolated normals
	normals = std::vector<glm::vec3>(idx);
	for (int ii = 0; ii < frequency + 1; ++ii) {
		for (int jj = 0; jj < frequency + 1; ++jj) {

			glm::vec3 normal;
			for (auto& el : nonInterpNormals[ii][jj]) {
				normal += el.second;
			}
			normal /= nonInterpNormals[ii][jj].size();
			
			for (auto& el : nonInterpNormals[ii][jj]) {
				normals[el.first] = normal;
			}
		}
	}

	DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

	DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

	MeshPtr mesh = std::make_shared<Mesh>();
	mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
	mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
	mesh->setPrimitiveType(GL_TRIANGLES);
	mesh->setVertexCount(vertices.size());

	return mesh;
}