include_directories(common)

set(EXAMPLES_REPO "./")

set(SRC_FILES
    ${EXAMPLES_REPO}/common/Application.cpp
        ${EXAMPLES_REPO}/common/DebugOutput.cpp
    ${EXAMPLES_REPO}/common/Camera.cpp
    ${EXAMPLES_REPO}/common/ShaderProgram.cpp
    Mesh.cpp
    TreeApplication.cpp
    Main.cpp
    LSystem.cpp
)

set(HEADER_FILES
    TreeApplication.h
    Mesh.hpp
    LSystem.h
    ${EXAMPLES_REPO}/common/Application.hpp
        ${EXAMPLES_REPO}/common/DebugOutput.h
    ${EXAMPLES_REPO}/common/Camera.hpp
    ${EXAMPLES_REPO}/common/ShaderProgram.hpp
)

MAKE_TASK(492Rakcheev 1 "${SRC_FILES}")

