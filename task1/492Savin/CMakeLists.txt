set(SRC_FILES
    main.cpp
    common/Application.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp

    common/Application.hpp
    common/Camera.hpp
    common/Mesh.hpp
    common/ShaderProgram.hpp
)


set(SHADER_FILES
    492SavinData/shader.vert
    492SavinData/shader.frag
)

source_group("Shaders" FILES
    ${SHADER_FILES}
)

include_directories(common)

MAKE_TASK(492Savin 1 "${SRC_FILES}")
