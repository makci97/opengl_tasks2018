#include "PerlinNoise.h"

PerlinNoise::PerlinNoise(unsigned int num_of_octaves, float persistence):
        num_of_octaves(num_of_octaves), persistence(persistence) {
}

float PerlinNoise::noise(int x, int y) {
    int n;
    n = x * 3  + y * 2;
    n = (n<<3) ^ n;
    return ( 1.0 - ( (n * (n * n * 15731 + 789221) + 1376312589) & 2147483647) / 3073741824.0);
}

float PerlinNoise::cosine_interpolation(float a, float b, float x) {
    float ft = x * 3.1415927f;
    float f = (1.f - cos(ft)) * .5f;
    return a * (1.f - f) + b * f;
}

float PerlinNoise::smoothed_noise(float x, float y) {
    float corners, sides, center;
    corners = (noise(x - 1, y - 1) + noise(x + 1, y - 1) + noise(x - 1, y + 1) + noise(x + 1, y + 1)) / 16;
    sides   = (noise(x - 1, y) + noise(x + 1, y) + noise(x, y - 1) + noise(x, y + 1)) / 8;
    center  =  noise(x, y) / 4;
    return corners + sides + center;
}

float PerlinNoise::interpolated_noise(float x, float y) {
    int integer_X    = int(x);
    float fractional_X = x - integer_X;
    int integer_Y    = int(y);
    float fractional_Y = y - integer_Y;

    float v1 = smoothed_noise(integer_X, integer_Y);
    float v2 = smoothed_noise(integer_X + 1, integer_Y);
    float v3 = smoothed_noise(integer_X, integer_Y + 1);
    float v4 = smoothed_noise(integer_X + 1, integer_Y + 1);

    float i1 = cosine_interpolation(v1, v2, fractional_X);
    float i2 = cosine_interpolation(v3, v4, fractional_X);
    return cosine_interpolation(i1, i2, fractional_Y);
}

float PerlinNoise::perlin_noise_2D(float x, float y) {
    int n = num_of_octaves;
    float p = persistence;
    float total = 0.f;
    float frequency;
    float amplitude;

    for (int i = 0; i < n; i++) {
        frequency = 2 * i;
        amplitude = p * i;
        total = total + interpolated_noise(x * frequency, y * frequency) * amplitude;
    }
    return total;
}
