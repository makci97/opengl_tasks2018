#include "TreeApplication.h"


int main()
{
    TreeApplication app;
    std::map<char, std::string> rules;
    // rules['F'] = "F[-F]<[F]>[+F][F]";
    rules['F'] = "F[-F][^F][&F][+F][F]";
    // rules['F'] = "F[-F]F[+F][F]";
    std::string initialString = "F";
    float stepDistance = 0.7;
    float rotateAngle = 50;
    float thickness = 0.07;
    float distanceScale = 0.8;
    float angleScale = 1;
    float thicknessScale = 0.6;
    uint16_t numIterations = 4;
    LSystem system(
        rules,
        initialString,
        stepDistance,
        rotateAngle,
        thickness,
        distanceScale,
        angleScale,
        thicknessScale
    );

    app.setupLSystem(
        system,
        numIterations
    );
    app.start();

    return 0;
}
